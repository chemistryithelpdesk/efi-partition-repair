# EFI Partition Repair, by UW-Madison Chemistry Dept. IT Helpdesk

This repository contains a script written by the by the UW-Madison Department of Chemistry IT Helpdesk to better automate the task of reformatting and repairing EFI boot partitions.

Intended use is from an external drive, specifically one containing the Windows recovery tool.